# Changelog

## v1.2.0

* update P7
* remove strong dependency from XFW

## v1.1.0

- global refactoring
- switch to XFW.Native/d3dhook for D3D9/DXGI hooking

## v1.0.0

- first version
