"""
This file is part of the XVM Framework project.

Copyright (c) 2017-2021 XVM Framework contributors.
Copyright (c) 2017-2021 XFW Performance Monitor contributors.

XVM Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XVM Framework is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import config
from telemetry import g_telemetry as logger

from events import registerEvent

import BigWorld
from gui.battle_control.controllers.debug_ctrl import DebugController

channel_id = None
counter_ping = None
counter_lag = None

if config.get('enabled', False) and config.get('features/network', False):
    counter_ping = logger.telemetry_counter_createfind(u'Telemetry', u'Network/Ping', 0, 0, 999, 100)
    counter_lag = logger.telemetry_counter_createfind(u'Telemetry', u'Network/Lag', 0, 0, 1, 1)

@registerEvent(DebugController, '_update')
def DebugController__update(self):
    if counter_ping is not None:
        logger.telemetry_sample_add(u'Telemetry', counter_ping, int(BigWorld.statPing()))

    if counter_lag is not None:
        logger.telemetry_sample_add(u'Telemetry', counter_lag, int(BigWorld.statLagDetected()))
