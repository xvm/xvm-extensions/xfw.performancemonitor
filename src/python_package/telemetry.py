"""
This file is part of the XVM Framework project.

Copyright (c) 2017-2021 XFW Performance Monitor contributors.

XVM Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XVM Framework is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import imp
import os
import traceback
import sys

import config
import vfs


class Telemetry(object):

    __native = None

    def __init__(self):
        self.__load_native()

    def __load_native(self):
        try:
            if self.__native is None:

                if "python27" not in sys.modules:
                    print "[XFW/Perfmon][__load_native] libpython was not found"
                    return False

                package_id = 'com.modxvm.xfw.perfmon'
                path_realfs = 'res_mods/mods/xfw_packages/xfw_perfmon/native/xfw_perfmon.pyd'
                path_vfs = 'mods/xfw_packages/xfw_perfmon/native/'

                if os.path.isfile(path_realfs):
                    self.__native = imp.load_dynamic('XFW_Perfmon', path_realfs)
                else:
                    path_realfs= 'mods\\temp\\%s\\native\\' % package_id
                    vfs.directory_copy(path_vfs, path_realfs)
                    self.__native = imp.load_dynamic('XFW_Perfmon', os.path.join(path_realfs,'xfw_perfmon.pyd'))

            return True
        except Exception:
            print "[XFW/Perfmon][__load_native] Error"
            traceback.print_exc()
            print "======================="

    def get_connection_string(self):
        """
        Build Baical/P7 connection string from XVM config file
        """
        try:
            c = config.get('baical_connection')
            return unicode(('/P7.Sink='    +c['sink']+
                            ' /P7.Name='   +c['name']+
                            ' /P7.Pool='   +str(c['memoryPool'])+
                            ' /P7.Addr='   +c['serverAddress']+
                            ' /P7.Port='   +str(c['serverPort'])+
                            ' /P7.PSize='  +str(c['packetSize'])+
                            ' /P7.Window=' +str(c['packetWindow'])+
                            ' /P7.Format="'+c['format']+'"'+
                            ' /P7.Dir='    +c['logDirectory']+
                            ' /P7.Roll='   +c['logRolling']+
                            ' /P7.Files='  +str(c['logMaxFiles'])))
        except Exception:
            print "[XFW/Perfmon][Telemetry/get_connection_string] Error"
            traceback.print_exc()
            print "======================="
            print "[XFW/Perfmon][Telemetry/get_connection_string] Trying to default connection string"
            return unicode('/P7.Sink=Auto /P7.Name=WoT_XFW /P7.Dir=logs_xfw')

    def connect(self):
        """
        Connect P7 to Baical
        """
        if self.__native is not None:
            self.__native.connect(self.get_connection_string())

    def enable_hooks(self):
        """
        Enable native telemetry hooks
        """
        if self.__native is not None:
            if config.get('features/d3d9', False):
                self.__native.d3d9_enable()

            if config.get('features/d3d11', False):
                self.__native.d3d11_enable()

    def disable_hooks(self):
        """
        Disable native telemetry hooks
        """
        if self.__native is not None:
            self.__native.d3d9_disable()
            self.__native.d3d11_disable()

    def telemetry_channel_createfind(self, channel_name):
        if self.__native is not None:
            return self.__native.telemetry_channel_createfind(unicode(channel_name))

    def telemetry_counter_createfind(self, channel_name, counter_name, counter_min, counter_alarm_min, counter_max, counter_alarm_max):
        if self.__native is not None:
            return self.__native.telemetry_counter_createfind(unicode(channel_name), unicode(counter_name), counter_min, counter_alarm_min, counter_max, counter_alarm_max)

    def telemetry_sample_add(self, channel_name, counter_id, counter_value):
        if self.__native is not None:
            return self.__native.telemetry_sample_add(unicode(channel_name), counter_id, counter_value)

    def trace_channel_createfind(self, channel_name):
        if self.__native is not None:
            return self.__native.trace_channel_createfind(unicode(channel_name))

#####################################################################
# initialization

g_telemetry = None

try:
    if config.get('enabled', False):
        g_telemetry = Telemetry()
        g_telemetry.connect()
        g_telemetry.enable_hooks()
except Exception:
    print "[XFW/Perfmon][Telemetry] Error when loading"
    traceback.print_exc()
    print "======================="
