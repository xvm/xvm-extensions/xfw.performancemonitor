"""
This file is part of the XFW Performance Monitor project.

Copyright (c) 2017-2021 XFW Performance Monitor contributors.

XVM Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XVM Framework is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import collections
import json
import os
import traceback

from ResMgr import openSection, isDir, isFile

import vfs

g_config = None

__all__ = ['get']

class Config:
    data = None

    def __init__(self, config_file):
        self.load(config_file)

    def load(self, config_file):
        try:
            with open(config_file) as json_file:
                self.data = json.load(json_file)
        except Exception:
            traceback.print_exc()

    def get(self, path, default):
        if not path or path == '':
            return default

        path = path.replace('.', '/')

        if path[0] == '/':
            path = path[1:]

        c = self.data
        for x in path.split('/'):
            if not isinstance(c, collections.Mapping) or x not in c:
                return default
            c = c[x]

        return c

def get(path, default=None):
    global g_config
    if g_config is None:
        return default

    return g_config.get(path, default)

#################

CONFIG_FILE_RFS = './mods/configs/com.modxvm.xfw.perfmon/performance_monitor.json'
CONFIG_FILE_VFS = 'mods/xfw_packages/xfw_perfmon/config/performance_monitor.json'

if not os.path.exists(CONFIG_FILE_RFS):
    vfs.file_copy(CONFIG_FILE_VFS, CONFIG_FILE_RFS)

g_config = Config(CONFIG_FILE_RFS)
