/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2017-2021 XFW Performance Monitor contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <map>
#include <memory>
#include <vector>
#include <string>

#include "P7_Client.h"
#include "P7_Trace.h"
#include "P7_Telemetry.h"

struct TraceChannel
{
    IP7_Trace* object = nullptr;
    std::map<std::wstring, uint32_t> threads;
    std::map<std::wstring, IP7_Trace::hModule> modules;
};

struct TelemetryChannel
{
    IP7_Telemetry* object = nullptr;

    bool isValid()
    {
        return object != nullptr;
    }
};

class Logger
{
private:
    IP7_Client* client = nullptr;

    std::map<std::wstring, std::shared_ptr<TelemetryChannel>> telemetry;
    std::map<std::wstring, std::shared_ptr<TraceChannel>> trace;

    bool LoadP7();

public:
    Logger();
    ~Logger();

    //connection
    bool connect(const std::wstring& connection_string);

    //telemetry/channel
    bool telemetry_channel_delete(const std::wstring& channel_name);
    std::shared_ptr<TelemetryChannel> telemetry_channel_createfind(const std::wstring& channel_name);

    //telemetry/counter
    uint16_t telemetry_counter_createfind(const std::wstring& channel_name, const std::wstring& counter_name, int64_t min, int64_t alarm_min, int64_t max, int64_t alarm_max);

    //telemetry/sample
    bool telemetry_sample_add(const std::wstring& channel, uint8_t counter_id, int64_t counter_value);

    /*


    //trace
    TraceChannel* trace_channel_createfind(std::wstring channel_name);
    std::map<std::wstring, TraceChannel*>::iterator trace_channel_delete(std::map<std::wstring, TraceChannel*>::iterator& iterator);
    bool trace_channel_delete(std::wstring channel_name);
    bool trace_thread_register(TraceChannel* channel, std::wstring thread_name, uint32_t thread_id);
    bool trace_thread_register(std::wstring channel_name, std::wstring thread_name, uint32_t thread_id);
    bool trace_thread_unregister(TraceChannel* channel, uint32_t thread_id);
    bool trace_thread_unregister(std::wstring channel_name, uint32_t thread_id);
    bool trace_thread_unregister(TraceChannel* channel, std::wstring thread_name);
    bool trace_thread_unregister(std::wstring channel_name, std::wstring thread_name);

    TelemetryChannel* telemetry_channel_createfind(std::wstring channel_name);
   */

};
