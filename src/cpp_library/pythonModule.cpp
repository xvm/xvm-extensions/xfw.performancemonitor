/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2017-2021 XFW Performance Monitor contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "pythonModule.h"


#include "Python.h"

#include "logger.h"
#include "counter_d3d9.h"
#include "counter_d3d11.h"

HINSTANCE hModule = nullptr;

class PythonModule;
PythonModule* pm = nullptr;

class PythonModule
{
private:

    Logger* logger = nullptr;
    D3D9FrametimeCounter* counter_d3d9 = nullptr;
    D3D11FrametimeCounter* counter_d3d11 = nullptr;

public:
    PythonModule()
    {
        logger = new Logger();
        counter_d3d9 = new D3D9FrametimeCounter(logger);
        counter_d3d11 = new D3D11FrametimeCounter(logger);
    }

    ~PythonModule()
    {
        delete counter_d3d9;
        delete counter_d3d11;
        delete logger;
    }


    static PyObject* py_connect(PyObject* self, PyObject* args)
    {
        wchar_t* connectionString;

        if (!PyArg_ParseTuple(args, "u", &connectionString))
            return nullptr;

        if (pm == nullptr)
            pm = new PythonModule();

        if (pm->logger->connect(connectionString))
            Py_RETURN_TRUE;

        Py_RETURN_FALSE;
    }

    static PyObject* py_d3d9_enable(PyObject* self, PyObject* args)
    {
        if (pm == nullptr)
            pm = new PythonModule();

        pm->counter_d3d9->Connect();

        Py_RETURN_NONE;
    }

    static PyObject* py_d3d9_disable(PyObject* self, PyObject* args)
    {
        Py_RETURN_NONE;
    }

    static PyObject* py_d3d11_enable(PyObject* self, PyObject* args)
    {
        if (pm == nullptr)
            pm = new PythonModule();

        pm->counter_d3d11->Connect();

        Py_RETURN_NONE;
    }

    static PyObject* py_d3d11_disable(PyObject* self, PyObject* args)
    {
        Py_RETURN_NONE;
    }

    static PyObject* py_telemetry_channel_createfind(PyObject* self, PyObject* args)
    {
        wchar_t* str;
        if (!PyArg_ParseTuple(args, "u", &str))
            return nullptr;

        if (pm == nullptr)
            pm = new PythonModule();

        const auto ch = pm->logger->telemetry_channel_createfind(str);
        if (ch == nullptr)
            Py_RETURN_FALSE;

        Py_RETURN_TRUE;
    }

    static PyObject* py_telemetry_counter_createfind(PyObject* self, PyObject* args)
    {
        wchar_t* channel_name;
        wchar_t* counter_name;
        int32_t counter_min;
        int32_t counter_max;
        int32_t counter_alarm_min;
        int32_t counter_alarm_max;

        if (!PyArg_ParseTuple(args, "uuiiii", &channel_name, &counter_name, &counter_min, &counter_alarm_min, &counter_max, &counter_alarm_max))
            return nullptr;

        if (pm == nullptr)
            pm = new PythonModule();

        uint8_t id = pm->logger->telemetry_counter_createfind(channel_name, counter_name, counter_min, counter_alarm_min, counter_max, counter_alarm_max);

        if (id == 255)
            Py_RETURN_NONE;

        return Py_BuildValue("i", id);
    }

    static PyObject* py_telemetry_sample_add(PyObject* self, PyObject* args)
    {
        wchar_t* channel_name;
        int counter_id;
        int value;

        if (!PyArg_ParseTuple(args, "uii", &channel_name, &counter_id, &value))
            return nullptr;

        if (pm == nullptr)
            pm = new PythonModule();

        if (pm->logger->telemetry_sample_add(channel_name, counter_id, value))
            Py_RETURN_TRUE;

        Py_RETURN_FALSE;
    }

    static PyObject* py_trace_channel_createfind(PyObject* self, PyObject* args)
    {
        wchar_t* str = nullptr;
        TraceChannel* ch = nullptr;

        if (!PyArg_ParseTuple(args, "u", &str))
            return nullptr;

        //ch = logger->trace_channel_createfind(str);
        ch = nullptr;

        if (ch != nullptr)
            Py_RETURN_TRUE;

        Py_RETURN_FALSE;
    }

};

static PyMethodDef PyMethods[] = {
    { "connect", PythonModule::py_connect, METH_VARARGS, ""},

    { "d3d9_enable", PythonModule::py_d3d9_enable, METH_VARARGS, "" },
    { "d3d9_disable", PythonModule::py_d3d9_disable, METH_VARARGS, "" },

    { "d3d11_enable", PythonModule::py_d3d11_enable, METH_VARARGS, "" },
    { "d3d11_disable", PythonModule::py_d3d11_disable, METH_VARARGS, "" },

    { "telemetry_channel_createfind", PythonModule::py_telemetry_channel_createfind, METH_VARARGS, "" },
    { "telemetry_counter_createfind", PythonModule::py_telemetry_counter_createfind, METH_VARARGS, "" },
    { "telemetry_sample_add"        , PythonModule::py_telemetry_sample_add        , METH_VARARGS, "" },

    { "trace_channel_createfind"    , PythonModule::py_trace_channel_createfind    , METH_VARARGS, "" },

    { nullptr, nullptr, 0, nullptr}
};

BOOLEAN WINAPI DllMain(HINSTANCE hDllHandle, DWORD nReason, LPVOID Reserved)
{
    switch ( nReason )
    {
        case DLL_PROCESS_ATTACH:
            hModule = hDllHandle;
            break;
        case DLL_PROCESS_DETACH:
            break;
        default:
            break;
    }
    return TRUE;
}

PyMODINIT_FUNC initXFW_Perfmon()
{
    Py_InitModule("XFW_Perfmon", PyMethods);
}
