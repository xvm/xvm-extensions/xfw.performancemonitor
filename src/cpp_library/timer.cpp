/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2017-2021 XFW Performance Monitor contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "timer.h"

LARGE_INTEGER Timer::Frequency;

Timer::Timer()
{
    if(Timer::Frequency.QuadPart==0)
        QueryPerformanceFrequency(&Timer::Frequency);

    LastTick.QuadPart = 0;
}

LARGE_INTEGER Timer::GetLastTick()
{
    return LastTick;
}

LARGE_INTEGER Timer::GetTick()
{
    QueryPerformanceCounter(&LastTick);
    return LastTick;
}

int64_t Timer::GetDelta()
{
    LARGE_INTEGER Measurement, Delta;

    QueryPerformanceCounter(&Measurement);
    Delta.QuadPart = Measurement.QuadPart - LastTick.QuadPart;
    LastTick = Measurement;

    Delta.QuadPart *= 1000000;
    Delta.QuadPart /= Timer::Frequency.QuadPart;

    return Delta.QuadPart;
}
