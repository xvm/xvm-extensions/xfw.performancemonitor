/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2017-2021 XFW Performance Monitor contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>

#include <Windows.h>

class Timer
{
private:
    static LARGE_INTEGER Frequency;
    LARGE_INTEGER LastTick;
public:
    Timer();
    LARGE_INTEGER GetLastTick();
    LARGE_INTEGER GetTick();
    int64_t GetDelta();
};
