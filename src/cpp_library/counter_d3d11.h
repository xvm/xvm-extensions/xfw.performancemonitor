/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2018-2021 XFW Performance Monitor contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once

#include <dxgi.h>

#include "logger.h"
#include "timer.h"

class D3D11FrametimeCounter
{
public:
    D3D11FrametimeCounter(Logger* logger);
    ~D3D11FrametimeCounter();

    bool Connect();

private:
    static D3D11FrametimeCounter* pointer;

    Timer timer;
    Logger* logger;
    std::shared_ptr<TelemetryChannel> telemetry_channel;
    uint16_t telemetry_counter = 0xFFFF;

    static HRESULT __stdcall D3D11FrametimeCounter::HookRoutine(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags);
};
