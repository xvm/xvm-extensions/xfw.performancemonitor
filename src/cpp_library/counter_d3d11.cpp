/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2018-2021 XFW Performance Monitor contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "counter_d3d11.h"
#include "xfw_d3dhook.h"

D3D11FrametimeCounter* D3D11FrametimeCounter::pointer = nullptr;

D3D11FrametimeCounter::D3D11FrametimeCounter(Logger* logger)
{
    pointer = this;
    this->logger = logger;
}

D3D11FrametimeCounter::~D3D11FrametimeCounter()
{
    pointer = nullptr;
    DXGI_SwapChain_Present_Unsubscribe("xfw_perfmon");
}

HRESULT __stdcall D3D11FrametimeCounter::HookRoutine(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags)
{
    if (pointer->timer.GetLastTick().QuadPart == 0)
        pointer->timer.GetTick();
    else
        if (pointer->telemetry_channel != nullptr)
            pointer->telemetry_channel->object->Add(pointer->telemetry_counter, pointer->timer.GetDelta());

    return S_OK;
}

bool D3D11FrametimeCounter::Connect()
{
    telemetry_channel = logger->telemetry_channel_createfind(L"Telemetry");

    if (telemetry_channel == nullptr || !telemetry_channel->isValid())
        return false;

    bool success = telemetry_channel->object->Create(TM("Performance/Timeframe (D3D11)"), 0, 0, 60000, 30000, 1, &telemetry_counter) != FALSE;

    if (success)
        success = DXGI_SwapChain_Present_Subscribe("xfw_perfmon", D3D11FrametimeCounter::HookRoutine);

    return success;
}
