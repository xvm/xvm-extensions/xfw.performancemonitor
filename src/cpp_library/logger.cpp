/*
* This file is part of the XFW Performance Monitor project.
*
* Copyright (c) 2017-2021 XFW Performance Monitor contributors.
*
* XFW Performance Monitor is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW Performance Monitor is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <filesystem>

#include "logger.h"
#include "Shlwapi.h"
#include "pythonModule.h"

using namespace std::experimental;
//ctor/dtor

bool Logger::LoadP7()
{
    WCHAR Path[MAX_PATH];
    GetModuleFileNameW(hModule, Path, MAX_PATH);
    PathRemoveFileSpecW(Path);

    return !(LoadLibraryW((filesystem::path(Path) / L"P7x32.dll").wstring().c_str()) == nullptr);
}

Logger::Logger()
{
    LoadP7();
    P7_Set_Crash_Handler();
}

Logger::~Logger()
{
    for(auto& telemetry_channel : telemetry)
    {

    }

    for (auto& trace_channel : trace)
    {

    }

    if (client != nullptr)
    {
        client->Release();
        client = nullptr;
    }
}

//connection

bool Logger::connect(const std::wstring& connection_string)
{
    if (client != nullptr)
        return false;

    client = P7_Create_Client(connection_string.c_str());

    return client != nullptr;
}

//telemetry
bool Logger::telemetry_channel_delete(const std::wstring& channel_name)
{
    auto it = telemetry.find(channel_name);
    if (it != telemetry.end())
    {
        it->second->object->Release();
        telemetry.erase(it);
        return true;
    }

    return false;
}

std::shared_ptr<TelemetryChannel> Logger::telemetry_channel_createfind(const std::wstring& channel_name)
{
    const auto it = telemetry.find(channel_name);
    if (it != telemetry.end())
        return it->second;

    IP7_Telemetry* l_pTelemetry = P7_Create_Telemetry(client, channel_name.c_str(), nullptr);

    auto tc = std::make_shared<TelemetryChannel>();
    if (l_pTelemetry != nullptr)
    {
        tc->object = l_pTelemetry;
        telemetry.emplace(std::wstring(channel_name), tc);
    }

    return tc;
}

uint16_t Logger::telemetry_counter_createfind(const std::wstring& channel_name, const std::wstring& counter_name, int64_t min, int64_t alarm_min, int64_t max, int64_t alarm_max)
{
    std::shared_ptr<TelemetryChannel> channel;

    const auto it = telemetry.find(channel_name);
    if (it == telemetry.end())
        channel = telemetry_channel_createfind(channel_name);
    else
        channel = it->second;

    uint16_t id = 0xFFFF;

    if (channel == nullptr)
        return id;

    if (!channel->isValid())
        return id;

    if (channel->object->Find(counter_name.c_str(), &id))
        return id;

    if (channel->object->Create(counter_name.c_str(), min, alarm_min, max, alarm_max, 1, &id))
        return id;

    return id;
}

bool Logger::telemetry_sample_add(const std::wstring& channel_name, uint8_t counter_id, int64_t counter_value)
{
    auto it = telemetry.find(channel_name);

    if (it == telemetry.end())
        return false;

    if (it->second == nullptr)
        return false;

    if (!it->second->isValid())
        return false;

    return it->second->object->Add(counter_id, counter_value);
}

/*

//////// TRACE

TraceChannel* Logger::trace_channel_createfind(std::wstring channel_name)
{
    auto it = trace.find(channel_name);

    if (it != trace.end())
        return it->second;

    IP7_Trace* l_pTrace = P7_Create_Trace(client, channel_name.c_str(), nullptr);

    if (l_pTrace == nullptr)
        return nullptr;

    TraceChannel* tc = new TraceChannel();
    tc->object = l_pTrace;
    tc->modules = new std::map<std::wstring, IP7_Trace::hModule>();
    tc->threads = new std::map<std::wstring, uint32_t>();

    trace.emplace(channel_name, tc);

    return tc;
}

std::map<std::wstring, TraceChannel*>::iterator Logger::trace_channel_delete(std::map<std::wstring, TraceChannel*>::iterator& iterator)
{
    if (iterator->second)
    {
        if(iterator->second->modules) //-V807
        {
            //for (auto it = iterator->second->modules->begin(); it != iterator->second->modules->end();)
            {
                //TODO
            }
            delete iterator->second->modules;
        }

        if (iterator->second->threads)
        {
            for (auto it = iterator->second->threads->begin(); it != iterator->second->threads->end(); ++it)
            {
                if (iterator->second->object)
                    iterator->second->object->Unregister_Thread(it->second);
            }

            delete iterator->second->threads;
        }

        if (iterator->second->object)
        {
            iterator->second->object->Release();
            delete iterator->second->object;
        }

        delete iterator->second;
    }
    return trace.erase(iterator);
}

bool Logger::trace_channel_delete(std::wstring channel_name)
{
    auto it = trace.find(channel_name);
    if (it != trace.end())
    {
        trace_channel_delete(it);
        return true;
    }

    return false;
}

bool Logger::trace_thread_register(TraceChannel* channel, std::wstring thread_name, uint32_t thread_id)
{
    if (channel->object == nullptr)
        return false;

    uint32_t thid = thread_id;
    if(thread_id==0)
        thid = GetCurrentThreadId();

    const bool result = channel->object->Register_Thread(thread_name.c_str(),thid);
    if (result)
        channel->threads->emplace(thread_name, thid);

    return result;
}

bool Logger::trace_thread_register(std::wstring channel_name, std::wstring thread_name, uint32_t thread_id)
{
    auto it = trace.find(channel_name);

    if (it == trace.end())
        return false;

    return trace_thread_register(it->second, thread_name, thread_id);
}

bool Logger::trace_thread_unregister(TraceChannel* channel, uint32_t thread_id)
{
    if (channel == nullptr)
        return false;

    for (auto it = channel->threads->begin(); it != channel->threads->end(); ++it)
    {
        if (it->second == thread_id)
        {
            channel->threads->erase(it);
            break;
        }
    }

    return channel->object->Unregister_Thread(thread_id);
}

bool Logger::trace_thread_unregister(std::wstring channel_name, uint32_t thread_id)
{
    const auto it = trace.find(channel_name);

    if (it == trace.end())
        return false;

    return trace_thread_unregister(it->second, thread_id);
}

bool Logger::trace_thread_unregister(TraceChannel* channel, std::wstring thread_name)
{
    bool result = false;
    if (channel == nullptr || channel->threads == nullptr || channel->object == nullptr)
        return result;

    const auto it = channel->threads->find(thread_name);
    if(it!=channel->threads->end())
    {
        result = channel->object->Unregister_Thread(it->second);
        channel->threads->erase(it);
    }
    return result;
}

bool Logger::trace_thread_unregister(std::wstring channel_name, std::wstring thread_name)
{
    const auto it = trace.find(channel_name);

    if (it == trace.end())
        return false;

    return trace_thread_unregister(it->second, thread_name);
}
*/
