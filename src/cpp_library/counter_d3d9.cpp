/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2018-2021 XFW Performance Monitor contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "counter_d3d9.h"
#include "xfw_d3dhook.h"

D3D9FrametimeCounter* D3D9FrametimeCounter::pointer = nullptr;

D3D9FrametimeCounter::D3D9FrametimeCounter(Logger* logger)
{
    pointer = this;
    this->logger = logger;
}

D3D9FrametimeCounter::~D3D9FrametimeCounter()
{
    pointer = nullptr;
    D3D9_DelSubscriber("xfw_perfmon");
}

//Direct3DDevice9::EndScene
void  __stdcall D3D9FrametimeCounter::HookRoutine(IDirect3DDevice9* pDevice)
{
    if (pointer->timer.GetLastTick().QuadPart == 0)
        pointer->timer.GetTick();
    else
        if (pointer->telemetry_channel != nullptr)
            pointer->telemetry_channel->object->Add(pointer->telemetry_counter, pointer->timer.GetDelta());
}

bool D3D9FrametimeCounter::Connect()
{
    telemetry_channel = logger->telemetry_channel_createfind(L"Telemetry");

    if (telemetry_channel == nullptr || !telemetry_channel->isValid())
        return false;

    bool success = telemetry_channel->object->Create(TM("Performance/Timeframe (D3D9)"), 0, 0, 60000, 30000, 1, &telemetry_counter) != FALSE;

    if (success)
        success = D3D9_AddSubscriber("xfw_perfmon", HookRoutine);

    return success;
}
