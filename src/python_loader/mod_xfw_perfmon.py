"""
This file is part of the XFW Performance Monitor project.

Copyright (c) 2017-2021 XVW Framework contributors.
Copyright (c) 2017-2021 XFW Performance Montitor contributors.

XFW Performance Monitor is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XFW Performance Monitor is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

# detect XFW
xfw_found = False
try:
    import mod_xfw
    xfw_found = True
except:
    pass

# perform manual loading in case we are working without XFW
if not xfw_found:

    # try to find XFW.native library
    xfwnative_found = False
    try:
        import mod_xfw_native
        xfwnative_found = True
    except:
        pass

    # perform XFW.Perfmon loading if XFW.Native was found
    if xfwnative_found:
        import sys
        sys.path.insert(0, 'mods/xfw_packages')
        import xfw_perfmon.python
